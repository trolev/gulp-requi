# gulp-requi


## Install
npm install git+ssh://git@bitbucket.org/trolev/gulp-requi.git --save-dev

## Usage
Example: coffescript + js

main.js

```javascript
//= require scripts.coffee
//= require utils/script3.js
//= require utils/**/*.*
//= require utils/script1.js
//= require !utils/script4.js

    alert('main.js')
```

gulpfile.js

```javascript
var requi = require('gulp-requi');
var concat = require('gulp-concat');
var gulpif = require('gulp-if');
var gulpif = require('gulp-coffee');

gulp.task('js', function(){
  gulp.src(['assets/js/main.js'])
    .pipe(requi())
    .pipe(
      gulpif(
        /[.]coffee$/,
        plugins.coffee()
      )
    )
    .pipe(concat())
    .pipe(gulp.dest('dest/js/'));
});
```


